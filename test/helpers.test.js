const assert = require('assert');
const helpers = require('../src/helpers');
const L = require('../src/lambdas');

describe('to_integer', () => {
    describe('Church Numerals', () => {
        it('should return 0 given Church Numeral ZERO', () => {
            const zero = helpers.toInt(L.ZERO);
            assert.equal(zero, 0);
        });
        
        it('should return 1 given Church Numeral ONE', () => {
            const one = helpers.toInt(L.ONE);
            assert.equal(one, 1);
        });

        it('should return 2 given Church Numeral TWO', () => {
            const two = helpers.toInt(L.TWO);
            assert.equal(two, 2);
        });

        it('should return 3 given Church Numeral THREE', () => {
            const three = helpers.toInt(L.THREE);
            assert.equal(three, 3);
        });
    });

    describe('Lambdas', () => {
        describe('INC', ()  => {
            it('should return 3 given Church Numeral TWO', () => {
                const expected = helpers.toInt(L.INC(L.TWO))
                assert.equal(expected, 3);
            });

            it('should return 4 given Church Numeral THREE', () => {
                const expected = helpers.toInt(L.INC(L.THREE))
                assert.equal(expected, 4);
            });
        });

        describe('ADD', () => {
            it('should return 3 given Church L ONE and TWO', () => {
                const expected = helpers.toInt(L.ADD(L.ONE)(L.TWO));
                assert.equal(expected, 3);
            });
            it('should return 5 given Church L TWO and THREE', () => {
                const expected = helpers.toInt(L.ADD(L.TWO)(L.THREE));
                assert.equal(expected, 5);
            });
            it('should return 10 given Church L FIVE and FIVE', () => {
                const five = L.ADD(L.TWO)(L.THREE);
                const expected = helpers.toInt(L.ADD(five)(five));
                assert.equal(expected, 10);
            });
        });

        describe('MULT', () => {
            describe('commutative property', () => {
                it('should return 6 given Church L TWO and THREE in that order', () => {
                    const expectedResult = L.MULT(L.TWO)(L.THREE);
                    const expected = helpers.toInt(expectedResult);
                    assert.equal(expected, 6);
                });
                it('should return 6 given Church L THREE and TWO in that order', () => {
                    const expectedResult = L.MULT(L.THREE)(L.TWO);
                    const expected = helpers.toInt(expectedResult);
                    assert.equal(expected, 6);
                });
            });
            it('should return 72 given only Church L TWO and THREE ', () => {
                const six = L.MULT(L.TWO)(L.THREE);
                const twelve = L.MULT(L.TWO)(six);
                const expectedResult = L.MULT(six)(twelve);
                const expected = helpers.toInt(expectedResult);
                assert.equal(expected, 72);
            });
        });

        describe('PHI', () => {
            it('should return PAIR(4,2) given PAIR(3,2)', () => {
                const givenPair = L.PAIR(L.THREE)(L.TWO);
                const expectedResult = L.PHI(givenPair);
                const expected = helpers.toArray(expectedResult);
                assert.deepEqual(expected, [4,3]);
            });
        });

		describe('PRED', () => {
			it('should return 2 given Church Numeral THREE', () => {
				const expectedResult = L.PRED(L.THREE);
				const expected = helpers.toInt(expectedResult);
				assert.equal(expected, 2);
            });
            
            it('should return 9 given Church Numeral TEN', () => {
                const five = L.ADD(L.TWO)(L.THREE);
                const ten = L.ADD(five)(five);
                const expectedResult = L.PRED(ten);
				const expected = helpers.toInt(expectedResult);
				assert.equal(expected, 9);
			});
		});

		describe('PAIR', () => {
            let pair

            beforeEach(() => {
                pair = L.PAIR(L.TWO)(L.THREE);
            });

            describe('toArray', () => {
                it('should return [2,3] given PAIR(TWO, THREE)', () => {
                    const expected = helpers.toArray(pair);
                    assert.deepEqual(expected, [2,3]);
                });
            });

			describe('FIRST', () => {
				it('should return 2 given PAIR(TWO, THREE)', () => {
                    const expectedResult = pair(L.FIRST);
					const expected = helpers.toInt(expectedResult);
					assert.equal(expected, 2);
				});
            });

            describe('SECOND', () => {
				it('should return 3 given PAIR(TWO, THREE)', () => {
                    const expectedResult = pair(L.SECOND);
					const expected = helpers.toInt(expectedResult);
					assert.equal(expected, 3);
				});
			});
		});

        describe('SUB', () => {
            it('should return 1 given Church Numeral THREE and TWO in that order', () => {
                const expectedResult = L.SUB(L.THREE)(L.TWO);
                const expected = helpers.toInt(expectedResult);
                assert.equal(expected, 1);
            });
            it('should return 7 given Church Numeral TEN and THREE in that order', () => {
                const five = L.ADD(L.TWO)(L.THREE);
                const ten = L.ADD(five)(five);
                const expectedResult = L.SUB(ten)(L.THREE);
                const expected = helpers.toInt(expectedResult);
                assert.equal(expected, 7);
            });
            
        });

		describe('TRUE', () => {
			it('should return true', () => {
				const expected = helpers.toBool(L.TRUE);
				assert.equal(expected, true);
			});
		});

		describe('FALSE', () => {
			it('should return false', () => {
				const expected = helpers.toBool(L.FALSE);
				assert.equal(expected, false);
			});
		});

		describe.only('negative integers', () => {
			const minusOne = L.MINUSONE;
			const expected = helpers.toInt(minusOne);
			assert.equal(expected, -1);
		});
    });
});
