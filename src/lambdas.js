const ZERO = f => g => g
const ONE = f => g => f(g)
const TWO = f => g => f(f(g))
const THREE = f => g => f(f(f(g)))

const INC = numeral => f => x => f(numeral(f)(x))
const ADD = a => b => a(INC)(b)
const MULT = x => y => z => x(y(z))

const PAIR = a => b => z => z(a)(b)
const TRUE = a => b => a
const FALSE = a => b => b
const FIRST = TRUE
const SECOND = FALSE

// Transition Function
const PHI = p => z => z(INC(p(FIRST)))(p(FIRST))
const PRED = numeral => numeral(PHI)(PAIR(ZERO)(ZERO))(SECOND)
const SUB = a => b => b(PRED)(a)

const MINUSONE = num => PAIR(TRUE)(num)

module.exports = {
    ZERO,
    ONE,
    TWO,
    THREE,

	MINUSONE,

    INC,
    PRED,
    ADD,
    MULT,

    PAIR,
    FIRST,
    SECOND,
    PHI,

    PRED,
    SUB,

	TRUE,
	FALSE,
}
