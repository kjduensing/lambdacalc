function toInt(i) {
	console.log("INTEGER:", i);
	const isNegativeL = i(a => b => a);
	console.log("IS NEGATIVE LAMBDA:", isNegativeL);
	console.log("IS NEGATIVE:", toBool(isNegativeL));
	return 0;
}

// TODO: expand to list
function toArray(pair) {
    const first = pair(a => b => a);
    const rest = pair(a => b => b);
    return [toInt(first), toInt(rest)];
}

function toBool(bool) {
	console.log("BOOL:", bool);
	return bool(true)(false);
}

module.exports = {
    toInt,
    toArray,
	toBool,
}
